# Mario & Luigi WEBSITE : README

## Project

<br>

This project is an Ynov  project from web mobile module which consist in a cordova application development.
We need to implement Cordova, to be PWA friendly, that the application works, a custom offline page, set notification messages and to be original.

<br>


## Tasks done

<br>

| Tasks | Description | Status | Value |
|-------|-------------|--------|-------|
| "Utilisation de cordova" | Functional cordova application with plugins : | Done | 5pts |
|| ```Splash Screen``` : splash screen set with custom logo on a grey background for 2000ms| Done | - |
|| ```Battery``` : for the moment the battery is just logged on the terminal | Done | - |
|| ```Status Bar``` : the status bar is set in aqua to keep the same style as the rest of the application | Done | - |
|| ```In App Browser``` : In app browser allows the user to open a page from json and local storage (if the syntax is correct no controls are done yet) | Done | - |
|| ```Screen Orientation``` : the screen is locked on portrait because it's easier to manage and to read | Done | - |
|| ```Globalization``` : the plugin globalization is used to custom language on application launch if the system is FR will return FR else return default language as EN | Done | - |
|| ```File``` : used to load language files with the correct path, else an error occurred on android device failed to load file:///android_assets/www/storage/xxx.json xO xO xO | Done | - |
| "L’application est PWA friendly"  | Single html page using javascript and css styles based on cordova template | Yes | 5pts |
| "L’application correspond à l'énoncé et fonctionne"  | This is a top list application with 3 main views and an offline page that allows the user to create his own top in local storage | Yes | 4pts |
| "La page offline est designée" | The offline page works on browser device, on android with the plane mode files still to be loaded don't know why | Done | 2pts |
| "L’application peut recevoir des notifications"  | One signal was implemented and return an informational message on your browser to asks for notification authorizations | Yes | 1pt |
| "Initiative"  | Some application bonuses as been added | In progress | 3pts |
|| ```Cache management``` : the application keep files in cache to reuse it offline | Done | - |
|| ```Esthetic app and easy to use``` : A beautiful app with custom styles !! :D | Done | - |
|| ```Local storage v2``` : second version of local storage allows user to store a list of Tops on his phone next step adding delete option | In progress | - |
|| ```New functionalities``` : 3 buttons added allows the user to scroll to top smoothly, change the theme color and the card language to change language on the application in function of our preferences actually French and English are implemented| Done | - |
|| ```Large panel of home tops``` : A large list of tops on the homepage in french and english to keep this app fully useless (15 elements * 2 french and english) | Done | - |

<br>

## Author

<br>

![Ludovic Flament](./readme_locals/profile/Ludovic_FLAMENT.png)

<br>
<br>

## Links and references

<br>

### Project Management

<br>

* ```Gitlab (private) :``` https://gitlab.com/m4637/paymentapp

<br>

### Technical references

<br>

#### Cordova

* ```Cordova Doc :``` https://cordova.apache.org/docs/en/10.x/
* ```Cordova Plugin Doc :``` https://cordova.apache.org/plugins/

#### Other

* ```Local Storage :``` https://developer.mozilla.org/fr/docs/Web/API/Window/localStorage