// Variables
const ORIGIN_URL = `${location.protocol}//${location.host}`;
const OFFLINE_URL = "offline.html";

const CACHE_NAME = "offline-v8";
// Set files to keep in cache
const CACHED_FILES = [
  OFFLINE_URL, 
  `${ORIGIN_URL}/js/jquery.js`,
  `${ORIGIN_URL}/css/index.css`,
  `${ORIGIN_URL}/css/offline.css`,
  `${ORIGIN_URL}/js/index.js`,
  `${ORIGIN_URL}/img/logo.png`,
];

// OFFLINE PAGE
const sendOfflinePage = (resolve) => {
  caches.open(CACHE_NAME).then((cache) => {
    cache.match(OFFLINE_URL).then((cachedResponse) => {
      resolve(cachedResponse);
    });
  });
};

// Fetch
const respondWithFetchPromiseNavigate = (event) =>
  new Promise((resolve) => {
    event.preloadResponse
      .then((preloadResponse) => {
        if (preloadResponse) {
          resolve(preloadResponse);
        }

        // Always try the network first.
        fetch(event.request)
          .then((networkResponse) => {
            resolve(networkResponse);
          })
          // send cache offline.html
          .catch(() => sendOfflinePage(resolve));
      })
      .catch(() => sendOfflinePage(resolve));
  });


// CACHE MANAGEMENT
const deleteOldCaches = () => 
  new Promise((resolve) => {
    caches.keys().then((keys) => {
      Promise.all(
        keys.map((key) => {
          if(key !== CACHE_NAME) {
            caches.delete(key);
          }
        })
      ).finally(resolve);
    });
  });



const fetchSW = (event) => {
  // We only want to call event.respondWith() if this is a navigation request
  // for an HTML page.
  if (event.request.mode === "navigate") {
    event.respondWith(respondWithFetchPromiseNavigate(event));
  } else if (CACHED_FILES.includes(event.request.url)) {
    event.respondWith(caches.match(event.request));
  }
};

/////////////////////////////////////////////

// Activate
const waitUntilActivatePromise = () =>
new Promise((resolve) => {
    // If exists old caches delete them and keep the latest
    deleteOldCaches().then(() => {
        if ("navigationPreload" in self.registration) {
            self.registration.navigationPreload.enable().finally(resolve);
        }
    });
});

const activate = (event) => {
  event.waitUntil(waitUntilActivatePromise());
  // Tell the active service worker to take control of the page immediately.
  self.clients.claim();
};

/////////////////////////////////////////////

// Install
const waitUntilInstallationPromise = () =>
  new Promise((resolve) => {
    caches.open(CACHE_NAME).then((cache) => {
      cache.addAll(CACHED_FILES).then(resolve);
    });
  });


const installSW = (event) => {
  event.waitUntil(waitUntilInstallationPromise());
  // Force the waiting service worker to become the active service worker.
  self.skipWaiting();
};


/////////////////////////////////////////////

// Init
self.addEventListener("install", installSW);
self.addEventListener("activate", activate);
self.addEventListener("fetch", fetchSW);
