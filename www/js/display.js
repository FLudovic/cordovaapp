// global variable to manage lang in app
var langIncrementations = 0;
var languages = ["fr-FR", "en-EN"];
var langApp = "";

function setJsonToHtml(language) {
    var requestUrl = "";

    // Display the language in function of the language system or on click event
    switch(language) {
        case 'fr-FR':
            console.log('Setting language to Français');
            // controls if the application is launched on mobile device before setting url
            // on android path are different and data could be not retrieved
            if(!!window.cordova && (cordova.file != undefined)) {
                requestUrl = cordova.file.applicationDirectory + 'www/storage/fr/toptopless.json';
            } else {
                requestUrl = '../storage/fr/toptopless.json';
            }
            break;
        default:
            console.log('Setting language to default');
            if(!!window.cordova && (cordova.file != undefined)) {
                requestUrl = cordova.file.applicationDirectory + 'www/storage/toptopless.json';
            } else {
                requestUrl = '../storage/toptopless.json';
            }
            break;
    }

    // Request json file to display local data
    var request = new XMLHttpRequest();
    request.open('GET', requestUrl);
    request.responseType = 'json';
    request.send();

    // On request load if there is data display it in html
    request.onload = function() {
        console.log('Request response : ', request.response);
        if(request.response != null) {           
            // Get the card container element
            const cardContainer = document.getElementById("cardContainer");

            // Prevent the default language display to reload data
            if(cardContainer != null) {
                cardContainer.innerHTML = "";
            }
            // Split json in card elements displayable
            request.response.forEach((top, i) => {
                // Set the card div with card variables
                const cardTop = `

                <div class="card" style="background-image: url(${top.image});">
                    <div class="content">
                        <h2 class="title">${i+1}. ${top.title}</h2>
                        <h4 class="subtitle">${top.subtitle}</h4>
                        <p class="copy">${top.description}</p>
                        <button class="card-button" onclick="openInAppBrowser('${top.url}')">View Page</button>
                    </div>
                </div>
                
                `;

                // Append the container by cards to display
                cardContainer.innerHTML += cardTop; 
            });

        } else { console.log('An error has occurred while attempting to retrieve json data'); }
    };

    langApp = language; // Update global languages variables
    langIncrementations = languages.indexOf(langApp); // Update the language index
}

function setLocalStorageToHtml() {
    // Get the local storage from storage.js
    var localStorageItems = getLocalStorage();
    const jsonLocalStorageItem = JSON.parse(localStorageItems);

    if(localStorageItems) {

        // Get the card container element
        // const cardContainer = document.getElementById("cardContainer");
        const mtCardContainer = document.getElementById("mtCardContainer");

        // Split json in card elements displayable
        jsonLocalStorageItem.forEach((top, i) => {
            // Set the card div with card variables
            const cardTop = `
    
            <div class="card" style="background-image: url(${top.image});">
                <div class="content">
                    <h2 class="title">${i+1}. ${top.title}</h2>
                    <h4 class="subtitle">${top.subtitle}</h4>
                    <p class="copy">${top.description}</p>
                    <button class="card-button" onclick="openInAppBrowser('${top.url}')">View Page</button>
                </div>
            </div>
            
            `;
            // Append the container by cards to display
            // cardContainer.innerHTML += cardTop; 
            mtCardContainer.innerHTML += cardTop;
        });
    }
}


// When document is ready
$( document ).ready(function() {
    // Initialize the background color for darks vs light theme
    const body = document.getElementById('body');
    body.style.backgroundColor = "rgb(235, 235, 235)";

    // set local json to html
    setJsonToHtml();
    // set local storage data to html
    setLocalStorageToHtml();
});


$(".themeButton").click(() => {
    const body = document.getElementById('body');
    // change the background color by the opposite color
    if(body.style.backgroundColor == "rgb(235, 235, 235)") { body.style.backgroundColor = "rgb(18, 18, 18)"; }
    else { body.style.backgroundColor = "rgb(235, 235, 235)"; }
});


$("#langButton").click(() => {
    // reload json data with the good language
    langIncrementations += 1;
    // Prevent list loop and reset values
    if(langIncrementations == (languages.length)) { 
        langIncrementations = 0; langApp = languages[0]; 
    }
    // after click add one to the lang index to run languages list
    setJsonToHtml(languages[langIncrementations]);
});