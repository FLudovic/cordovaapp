// storage
submitFormButton = $('#submitFormButton');

submitFormButton.click(() => {
    // Get form variables
    formTitle = $('#formTitle').val();
    formSubtitle = $('#formSubtitle').val();
    formDescription = $('#formDescription').val();
    formUrl = $('#formUrl').val();
    formImage = $('#formImage').val();

    // Set top based on local storage
    var top = localStorage.getItem("topItem");
    
    // if local storage is null we initialize it
    if(top == null) {
        top = [{
            title: formTitle,
            subtitle: formSubtitle,
            image: formImage,
            description: formDescription,
            "url": formUrl
        }];
    // if local storage already contains some data we push it
    } else {
        // we parse the json variable to manage data and push it
        top = JSON.parse(top);
        top.push({
            "title": formTitle,
            "subtitle": formSubtitle,
            "image": formImage,
            "description": formDescription,
            "url": formUrl
        });
    }
    // set variable to string to store in local storage
    localStorage.setItem('topItem', JSON.stringify(top));
});

function getLocalStorage() {
    const cardTop = localStorage.getItem('topItem');
    console.log("Local Storage : ", cardTop);
    return cardTop;
}
