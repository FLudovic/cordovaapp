// Listeners
document.addEventListener('deviceready', onDeviceReady, false);


// On device ready set all parameters
function onDeviceReady() {
    console.log('[i] Running cordova-' + cordova.platformId + '@' + cordova.version);
    console.log('[i] Screen orientation : ' + screen.orientation.type);
    console.log('[i] File : ' + cordova.file);

    initFunctions();
}

function initFunctions() {
    window.addEventListener("batterystatus", onBatteryStatus, false);
    window.addEventListener("batterylow", onBatteryLow, false);
    window.addEventListener("batterycritical", onBatteryCritical, false);
    window.screen.orientation.lock("portrait");

    // Get language from the device to translate display
    navigator.globalization.getPreferredLanguage(
        function (language) {
            console.log('[i] language : ' + language.value);  
            setJsonToHtml(language.value);
        },
        function () { alert('Error getting language\n'); }
    );

}


// Cordova Component Functions
function onBatteryStatus(status) {
    console.log("[i] Level: " + status.level + " isPlugged: " + status.isPlugged);
}
function onBatteryLow(status) {
    alert("[i] Battery Level Low : " + status.level + "%");
}
function onBatteryCritical(status) {
    alert("[i] Battery Level Critical : " + status.level + "% Recharge Soon !");
}

// Open In App Browser Functions
function openInAppBrowser(url) {
    var options = "location=yes,zoom=false,fullscreen=yes";
    var target = "_blank";

    var request = cordova.InAppBrowser.open(url, target, options);

    request.addEventListener('loadstart', loadstartCallback);
    request.addEventListener('loadstop', loadstopCallback);
    request.addEventListener('loaderror', loaderrorCallback);
    request.addEventListener('exit', exitCallback);

    function loadstartCallback(event) {
        console.log('Loading started: '  + event.url);
    }

    function loadstopCallback(event) {
        console.log('Loading finished: ' + event.url);

    }

    function loaderrorCallback(error) {
        console.log('Loading error: ' + error.message);
    }

    function exitCallback() {
        console.log('Browser is closed...');
    }
}
