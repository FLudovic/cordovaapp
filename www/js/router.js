// Router
ROUTER = (() => {

    const divs = [$("#home"), $("#myTop"), $("#create")];
    const links = [$("#homeLink"), $("#myTopLink"), $("#createLink"), $(".createButton")];

    // todo forEach -> links[i] 

    const init = () => {
        divs[0].removeClass("hidden");
        
        // Home navbar
        links[0].click(() => {
            divs[0].removeClass("hidden");
            divs[1].addClass("hidden");
            divs[2].addClass("hidden");
        });
        // My Top navbar
        links[1].click(() => {
            divs[0].addClass("hidden");
            divs[1].removeClass("hidden");
            divs[2].addClass("hidden");
        });
        //  Create navbar
        links[2].click(() => {
            divs[0].addClass("hidden");
            divs[1].addClass("hidden");
            divs[2].removeClass("hidden");
        });
        // Create button
        links[3].click(() => {
            divs[0].addClass("hidden");
            divs[1].addClass("hidden");
            divs[2].removeClass("hidden");
        });
    };


    return { init };
})();

ROUTER.init();